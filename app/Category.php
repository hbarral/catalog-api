<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Category extends Model
{
    protected $fillable = ['name', 'description'];

    protected $hidden = ['user_id'];

    /**
     * @OA\Property(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @OA\Property(type="string")
     * @var string
     */
    protected $name;

    /**
     * @OA\Property(type="string", nullable=true)
     * @var string|null
     */
    protected $description;

    /**
     * @OA\Property(type="integer", writeOnly=true)
     * @var int
     */
    protected $user_id;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $created_at;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $updated_at;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTimestamps();
    }
}
