<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @OA\Tag(
 *    name="Product",
 *    description="Everything about Product API",
 * )
 */
class ProductController extends Controller
{
    /**
     * Create a new ProductController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt');
        auth()->setDefaultDriver('api');
    }

    /**
     * @OA\Get(
     *  path="/product",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="productAll",
     *  tags={"Product"},
     *  summary="Display a list of products.",
     *  @OA\Parameter(
     *   name="page",
     *   in="query",
     *   description="The page number",
     *   required=false,
     *
     *   @OA\Schema(
     *    type="integer"
     *   )
     *  ),
     *  @OA\Response(
     *    response="200",
     *    description="Return all products",
     *    @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Product"))
     *  )
     * )
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query()->paginate(10);

        return response()->json($products, 200);
    }

    /**
     * @OA\Post(
     *  path="/product",
     *  operationId="productCreate",
     *  tags={"Product"},
     *  summary="Create a new product.",
     *   security={
     *    {"bearerAuth": {}}
     *   },
     *   @OA\Response(
     *    response="201",
     *    description="The product has been saved.",
     *    @OA\JsonContent(ref="#/components/schemas/Product")
     *   ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Product Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="price",
     *                     description="Product Price",
     *                     type="float"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description of the product",
     *                     type="string"
     *                 ),
     *                 required={"name", "price"}
     *             )
     *         )
     *     )
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name' => 'required|min:3|max:255|string|unique:products',
        'price' => 'required|numeric',
        'categories' => 'array|min:1',
        'categories.*' => 'distinct|numeric'
      ]);

        $product = new Product();
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description ?? '';
        $product->user_id = auth()->user()->id;

        $product->save();

        if ($request->has('categories')) {
            $categories = Category::find($request->categories);
            $product->categories()->attach($categories);
        }

        return response()->json($product, 201);
    }

    /**
     * @OA\Get(
     *  path="/product/{productIdToGet}",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="productSingle",
     *  tags={"Product"},
     *  summary="Display a single product.",
     *  @OA\Parameter(
     *   name="productIdToGet",
     *   in="path",
     *   description="The product id",
     *   required=true,
     *
     *   @OA\Schema(
     *    type="integer"
     *   )
     *  ),
     *  @OA\Response(
     *    response="200",
     *    description="Return a single product",
     *      @OA\JsonContent(
     *        @OA\Property(
     *          property="id",
     *          type="number",
     *        ),
     *        @OA\Property(
     *          property="name",
     *          type="string"
     *        ),
     *        @OA\Property(
     *          property="price",
     *          type="number",
     *          format="float"
     *        ),
     *        @OA\Property(
     *          property="description",
     *          type="string"
     *        ),
     *        @OA\Property(
     *          property="categories",
     *          type="array",
     *           @OA\Items(
     *            ref="#/components/schemas/Category"
     *          )
     *        )
     *      )
     *    )
     *  ),
     *  @OA\Response(
     *   response="404",
     *   description="Product not found."
     *  )
     * )
     *
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Product::with('categories')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'product not found'], 404);
        } catch (NotFoundHttpException $e) {
            return response()->json(['error' => 'product not found'], 404);
        }

        return response()->json($product, 200);
    }

    /**
     * @OA\Post(
     *  path="/product/{productIdToUpdate}",
     *  tags={"Product"},
     *  summary="Update a single product.",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  @OA\Parameter(
     *      name="productIdToUpdate",
     *      description="Product id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="_method",
     *                     description="Method Spoofing",
     *                     type="string",
     *                     default="PUT",
     *                     readOnly=true
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="Product Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description of the product",
     *                     type="string"
     *                 ),
     *                 required={"_method"}
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="Return a single product",
     *      @OA\JsonContent(
     *        @OA\Property(
     *          property="id",
     *          type="number",
     *        ),
     *        @OA\Property(
     *          property="name",
     *          type="string"
     *        ),
     *        @OA\Property(
     *          property="price",
     *          type="number",
     *          format="float"
     *        ),
     *        @OA\Property(
     *          property="description",
     *          type="string"
     *        ),
     *        @OA\Property(
     *          property="categories",
     *          type="array",
     *           @OA\Items(
     *            ref="#/components/schemas/Category"
     *          )
     *        )
     *      )
     *    ),
     *  @OA\Response(
     *    response="404",
     *    description="Product not found"
     *  )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'name' => 'min:3|max:255|string',
        'price' => 'numeric',
        'categories' => 'array|min:1',
        'categories.*' => 'distinct|numeric'
      ]);

        try {
            $product = Product::with('categories')->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Product not found'], 404);
        }

        if ($product->user_id != auth()->user()->id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $product->name = $request->name ?? $product->name;
        $product->price = $request->price ?? $product->price;
        $product->description = $request->description ?? $product->description;

        $product->save();

        if ($request->has('categories')) {
            $categories = Category::find($request->categories);
            $product->categories()->attach($categories);
        }

        return response()->json($product, 200);
    }

    /**
     * @OA\Post(
     *  path="/product/{productIdToDelete}",
     *  tags={"Product"},
     *  summary="Delete a single product.",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  @OA\Parameter(
     *      name="productIdToDelete",
     *      description="Product id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="_method",
     *                     description="Method Spoofing",
     *                     type="string",
     *                     default="DELETE",
     *                     readOnly=true
     *                 )
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="The product was deleted.",
     *    @OA\JsonContent(ref="#/components/schemas/Product")
     *  ),
     *  @OA\Response(
     *    response="404",
     *    description="Product not found"
     *  ),
     *  @OA\Response(
     *    response="403",
     *    description="Forbidden"
     *  )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'product not found'], 404);
        }

        if ($product->user_id != auth()->user()->id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $product->categories()->detach();
        $product->delete();

        return response()->json($product, 200);
    }

    /**
     * Display a list of products by category id.
     *
     * @param  int  $categoryId
     * @return \Illuminate\Http\Response
     */
    public function getProductsByCategory($categoryId)
    {
        try {
            $category = Category::findOrFail($categoryId);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'category not found'], 404);
        }

        $products = $category->products;

        return response()->json($products, 200);
    }
}
