<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @OA\Tag(
 *    name="Authentication",
 *    description="Everything about Auth API"
 * )
 */
class AuthController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt', ['except' => ['login', 'register']]);
        auth()->setDefaultDriver('api');
    }


    /**
     * @OA\Post(
     *  path="/auth/login",
     *  operationId="login",
     *  tags={"Authentication"},
     *  summary="Get a JWT via given credentials.",
     *     @OA\RequestBody(
     *         description="Login",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="email",
     *                     description="Your email",
     *                     type="string",
     *                     format="email",
     *                     example="john.doe@mail.ru"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Your password",
     *                     type="string",
     *                     format="password",
     *                     example="1234567890"
     *                 ),
     *                 required={"email", "password"}
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="Return a token",
     *    @OA\JsonContent(
     *            @OA\Property(
     *            property="access_token",
     *            type="string",
     *            example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...."
     *            ),
     *            @OA\Property(
     *              property="token_type",
     *              type="string",
     *              example="bearer"
     *            ),
     *            @OA\Property(
     *              property="expires_in",
     *              type="number",
     *              format="int64",
     *              example="86400"
     *            )
     *    )
     *  ),
     *  @OA\Response(
     *    response="401",
     *    description="Unauthorized",
     *    @OA\JsonContent(
     *            @OA\Property(
     *            property="error",
     *            type="string",
     *            example="Unauthorized"
     *            )
     *        )
     *    )
     * )
     *
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * @OA\Post(
     *  path="/auth/register",
     *  operationId="register",
     *  tags={"Authentication"},
     *  summary="Register a new user",
     *     @OA\RequestBody(
     *         description="Register",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Your name",
     *                     type="string",
     *                     example="John Doe"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Your email",
     *                     type="string",
     *                     format="email",
     *                     example="john.doe@mail.ru"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Your password",
     *                     type="string",
     *                     format="password",
     *                     example="1234567890"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Confirm your password",
     *                     type="string",
     *                     format="password",
     *                     example="1234567890"
     *                 ),
     *                 required={"name", "email", "password", "password_confirmation"}
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="201",
     *    description="Return a new user with token",
     *    @OA\JsonContent(
     *          @OA\Property(
     *            property="user",
     *            ref="#/components/schemas/User",
     *          ),
     *          @OA\Property(
     *            property="access_token",
     *            type="string",
     *            example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...."
     *          )
     *       )
     *    ),
     *  @OA\Response(
     *    response="400",
     *    description="Bad request",
     *      @OA\JsonContent(
     *        @OA\Property(
     *          property="property",
     *          type="array",
     *          @OA\Items(
     *            example="Error description"
     *          )
     *       )
     *     )
     *   )
     * )
     *
     * Create a new user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
      ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create([
        'name' => $request->get('name'),
        'email' => $request->get('email'),
        'password' => Hash::make($request->get('password')),
      ]);

        $token = JWTAuth::fromUser($user);

        return response()->json(compact('user', 'token'), 201);
    }

    /**
     * @OA\Post(
     *  path="/auth/update",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="update",
     *  tags={"Authentication"},
     *  summary="Update current user details",
     *     @OA\RequestBody(
     *         description="Update",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Your name",
     *                     type="string",
     *                     example="John Doe"
     *                 ),
     *                 @OA\Property(
     *                     property="email",
     *                     description="Your email",
     *                     type="string",
     *                     format="email",
     *                     example="john.doe@mail.ru"
     *                 ),
     *                 @OA\Property(
     *                     property="password",
     *                     description="Your password",
     *                     type="string",
     *                     format="password",
     *                     example="1234567890"
     *                 ),
     *                 @OA\Property(
     *                     property="password_confirmation",
     *                     description="Confirm your password",
     *                     type="string",
     *                     format="password",
     *                     example="1234567890"
     *                 ),
     *                 required={"name", "email", "password", "password_confirmation"}
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="Return a user",
     *    @OA\JsonContent(
     *          @OA\Property(
     *            property="user",
     *            ref="#/components/schemas/User",
     *          )
     *       )
     *    ),
     *  @OA\Response(
     *    response="400",
     *    description="Bad request",
     *      @OA\JsonContent(
     *        @OA\Property(
     *          property="property",
     *          type="string",
     *          example="Error description"
     *       )
     *     )
     *   )
     * )
     *
     * Update a new user.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
        'name' => 'string|max:255',
        'email' => 'string|email|max:255',
        'password' => 'string|min:6|confirmed',
      ]);

        $user = auth()->user();
        $user->name = $request->name ?? auth()->user()->name;
        $user->email = $request->email ?? auth()->user()->email;

        if ($request->has('password')) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        return response()->json(['user' => $user], 200);
    }

    /**
     * @OA\Post(
     *  path="/auth/me",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="me",
     *  tags={"Authentication"},
     *  summary="Get current user details",
     *  @OA\Response(
     *    response="200",
     *    description="Return a current user details.",
     *    @OA\JsonContent(
     *            ref="#/components/schemas/User",
     *       )
     *    ),
     *  @OA\Response(
     *    response="400",
     *    description="Bad request",
     *      @OA\JsonContent(
     *        @OA\Property(
     *          property="property",
     *          type="array",
     *          @OA\Items(
     *            example="Error description"
     *          )
     *       )
     *     )
     *   )
     * )
     *
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * @OA\Post(
     *  path="/auth/logout",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="logout",
     *  tags={"Authentication"},
     *  summary="Log the user out (Invalidate the token).",
     *  @OA\Response(
     *    response="200",
     *    description="Logout the current user.",
     *    @OA\JsonContent(
     *       @OA\Property(
     *         property="message",
     *         example="Successfully logged out"
     *       )
     *     )
     *   ),
     *   @OA\Response(
     *     response="400",
     *     description="Bad Request",
     *     @OA\JsonContent(
     *        @OA\Property(
     *         property="error",
     *         example="Error description"
     *       )
     *     )
     *   )
     * )
     *
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @OA\Post(
     *  path="/auth/refresh",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="refresh",
     *  tags={"Authentication"},
     *  summary="Renew the current token",
     *  @OA\Response(
     *    response="200",
     *    description="Return a new token",
     *    @OA\JsonContent(
     *            @OA\Property(
     *            property="access_token",
     *            type="string",
     *            example="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9...."
     *            ),
     *            @OA\Property(
     *              property="token_type",
     *              type="string",
     *              example="bearer"
     *            ),
     *            @OA\Property(
     *              property="expires_in",
     *              type="number",
     *              format="int64",
     *              example="86400"
     *            )
     *    )
     *  ),
     *  @OA\Response(
     *    response="400",
     *    description="Bad request",
     *    @OA\JsonContent(
     *          @OA\Property(
     *            property="error",
     *            type="string",
     *            example="Error description"
     *          )
     *       )
     *    )
     * )
     *
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @OA\Post(
     *  path="/auth/payload",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="payload",
     *  tags={"Authentication"},
     *  summary="Get details about the current token",
     *  @OA\Response(
     *    response="200",
     *    description="Return a payload",
     *    @OA\JsonContent(
     *            @OA\Property(
     *            property="iss",
     *            type="string",
     *            example="issuer url"
     *            ),
     *            @OA\Property(
     *              property="iat",
     *              type="number",
     *              example="1569586179"
     *            ),
     *            @OA\Property(
     *              property="exp",
     *              type="number",
     *              example="1569672579"
     *            ),
     *            @OA\Property(
     *              property="nbf",
     *              type="number",
     *              example="1569586179"
     *            ),
     *            @OA\Property(
     *              property="jti",
     *              type="number",
     *              example="FlhxzZn4ZOqJ9CSa"
     *            ),
     *            @OA\Property(
     *              property="sub",
     *              type="number",
     *              example="4"
     *            ),
     *            @OA\Property(
     *              property="prv",
     *              type="string",
     *              example="87e0af1ef9fd15812fdec97153a14e0b047546aa"
     *            ),
     *    )
     *  ),
     *  @OA\Response(
     *    response="400",
     *    description="Bad request",
     *    @OA\JsonContent(
     *          @OA\Property(
     *            property="error",
     *            type="string",
     *            example="Error description"
     *          )
     *       )
     *    )
     * )
     *
     * Get the raw JWT payload.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function payload()
    {
        return response()->json(
            auth()->payload()
      );
    }
}
