<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * @OA\Tag(
 *    name="Category",
 *    description="Everything about Category API",
 * )
 */
class CategoryController extends Controller
{
    /**
     * Create a new CategoryController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt');
        auth()->setDefaultDriver('api');
    }

    /**
     * @OA\Get(
     *  path="/category",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="categoriesAll",
     *  tags={"Category"},
     *  summary="Display a list of categories.",
     *  @OA\Parameter(
     *   name="page",
     *   in="query",
     *   description="The page number",
     *   required=false,
     *
     *   @OA\Schema(
     *    type="integer"
     *   )
     *  ),
     *  @OA\Response(
     *    response="200",
     *    description="Return all categories",
     *    @OA\JsonContent(type="array", @OA\Items(ref="#/components/schemas/Category"))
     *  )
     * )
     *
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::query()->paginate(10);

        return response()->json($categories, 200);
    }

    /**
     * @OA\Post(
     *  path="/category",
     *  operationId="categoryCreate",
     *  tags={"Category"},
     *  summary="Create a new category.",
     *   security={
     *    {"bearerAuth": {}}
     *   },
     *   @OA\Response(
     *    response="201",
     *    description="The category has been saved."
     *   ),
     *
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="name",
     *                     description="Category Name",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description of the category",
     *                     type="string"
     *                 ),
     *                 required={"name"}
     *             )
     *         )
     *     )
     * )
     *
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
        'name' => 'required|min:3|max:255|string|unique:categories',
      ]);

        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description ?? '';
        $category->user_id = auth()->user()->id;

        $category->save();

        return response()->json($category, 201);
    }

    /**
     * @OA\Get(
     *  path="/category/{categoryIdToGet}",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  operationId="categorySingle",
     *  tags={"Category"},
     *  summary="Display a single category.",
     *  @OA\Parameter(
     *   name="categoryIdToGet",
     *   in="path",
     *   description="The category id",
     *   required=true,
     *
     *   @OA\Schema(
     *    type="integer"
     *   )
     *  ),
     *  @OA\Response(
     *    response="200",
     *    description="Return a single category",
     *    @OA\JsonContent(ref="#/components/schemas/Category")
     *  ),
     *  @OA\Response(
     *   response="404",
     *   description="Category not found."
     *  )
     * )
     *
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        return response()->json($category, 200);
    }

    /**
     * @OA\Post(
     *  path="/category/{categoryIdToUpdate}",
     *  tags={"Category"},
     *  summary="Update a single category.",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  @OA\Parameter(
     *      name="categoryIdToUpdate",
     *      description="Category id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="_method",
     *                     description="Method Spoofing",
     *                     type="string",
     *                     default="PUT",
     *                     readOnly=true
     *                 ),
     *                 @OA\Property(
     *                     property="name",
     *                     description="Category Name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     property="description",
     *                     description="Description of the category",
     *                     type="string"
     *                 ),
     *                 required={"_method"}
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="The category was updated.",
     *    @OA\JsonContent(ref="#/components/schemas/Category")
     *  ),
     *  @OA\Response(
     *    response="404",
     *    description="Category not found"
     *  )
     * )
     *
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
        'name' => 'min:3|max:255|string'
      ]);

        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        if ($category->user_id != auth()->user()->id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $category->name = $request->name ?? $category->name;
        $category->description = $request->description ?? $category->description;

        $category->save();

        return response()->json($category, 200);
    }

    /**
     * @OA\Post(
     *  path="/category/{categoryIdToDelete}",
     *  tags={"Category"},
     *  summary="Delete a single category.",
     *  security={
     *    {"bearerAuth": {}}
     *  },
     *  @OA\Parameter(
     *      name="categoryIdToDelete",
     *      description="Category id",
     *      required=true,
     *      in="path",
     *      @OA\Schema(
     *          type="integer"
     *      )
     *  ),
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 type="object",
     *                 @OA\Property(
     *                     property="_method",
     *                     description="Method Spoofing",
     *                     type="string",
     *                     default="DELETE",
     *                     readOnly=true
     *                 )
     *             )
     *         )
     *     ),
     *  @OA\Response(
     *    response="200",
     *    description="The category was deleted.",
     *    @OA\JsonContent(ref="#/components/schemas/Category")
     *  ),
     *  @OA\Response(
     *    response="404",
     *    description="Category not found"
     *  ),
     *  @OA\Response(
     *    response="403",
     *    description="Forbidden"
     *  )
     * )
     *
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Category not found'], 404);
        }

        if ($category->user_id != auth()->user()->id) {
            return response()->json(['error' => 'Forbidden'], 403);
        }

        $category->products()->detach();
        $category->delete();

        return response()->json(['category' => $category], 200);
    }
}
