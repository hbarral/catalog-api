<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *     version="1.0.0",
 *     title="REST API for a product catalog",
 *     description="Simple REST API for a product catalog",
 *     @OA\Contact(
 *         email="hbarral@mail.ru"
 *     ),
 *
 *    @OA\License(
 *        name="Apache 2.0",
 *        url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *    )
 * )
 *
 *
 * @OA\Server(
 *    url=L5_SWAGGER_CONST_HOST,
 *    description="Simple REST API for a product catalog Server"
 * )
 *
 * @OA\SecurityScheme(
 *    type="apiKey",
 *    in="header",
 *    name="Authorization",
 *    bearerFormat="JWT",
 *    securityScheme="bearerAuth",
 *    description="Use a global email / password combo to obtain a token in Auth section. Put your token in the box below like this «Bearer [your-token]»",
 * )
 *
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
