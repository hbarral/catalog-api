<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @OA\Schema()
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @OA\Property(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @OA\Property(type="string")
     * @var string
     */
    protected $name;

    /**
     * @OA\Property(type="string", format="email")
     * @var string
     */
    protected $email;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $email_verified_at;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $created_at;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $updated_at;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
