<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema()
 */
class Product extends Model
{
    protected $fillable = ['name', 'price', 'description'];

    protected $hidden = ['user_id'];

    /**
     * @OA\Property(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @OA\Property(type="string")
     * @var string
     */
    protected $name;

    /**
     * @OA\Property(type="number", format="float")
     * @var float
     */
    protected $price;

    /**
     * @OA\Property(type="string", nullable=true)
     * @var string|null
     */
    protected $description;

    /**
     * @OA\Property(type="integer", writeOnly=true)
     * @var int
     */
    protected $user_id;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $created_at;

    /**
     * @OA\Property(type="string", format="date-time", nullable=true, readOnly=true)
     * @var \DateTimeInterface|null
     */
    protected $updated_at;

    /**
     * @OA\Property(
     * property="categories",
     * type="array",
     *    @OA\Items(
     *      @OA\Property(
     *        type="integer",
     *        property="id"
     *      ),
     *      @OA\Property(
     *        type="string",
     *        property="name"
     *      ),
     *      @OA\Property(
     *        type="string",
     *        property="description"
     *      )
     *    )
     * )
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class)->withTimestamps();
    }
}
