<?php

Route::group(
    [ 'prefix' => 'auth' ],
    function () {
        Route::post('login', 'Api\AuthController@login');
        Route::post('register', 'Api\AuthController@register');
        Route::post('logout', 'Api\AuthController@logout');
        Route::post('refresh', 'Api\AuthController@refresh');
        Route::post('me', 'Api\AuthController@me');
        Route::post('payload', 'Api\AuthController@payload');
        Route::post('update', 'Api\AuthController@update');
    }
);

Route::apiResource('category', 'Api\CategoryController');
Route::apiResource('product', 'Api\ProductController');
Route::get('product/category/{categoryId}', 'Api\ProductController@getProductsByCategory');
