FROM php:7.2-cli

RUN apt-get update -yq && apt-get install -yq \
      curl \
      && curl -L https://deb.nodesource.com/setup_16.x | bash

RUN apt-get install -yq \
      gnupg ca-certificates \
      libmcrypt-dev \
      libonig-dev \
      libpng-dev \
      libxml2-dev \
      libzip-dev \
      nodejs \
      unzip \
      zip \
      zlib1g-dev

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install \
      mbstring \
      mysqli \
      pdo \
      pdo_mysql \
      zip \
      && docker-php-ext-configure intl \
      && docker-php-ext-install intl \
      && docker-php-ext-configure gd \
      && docker-php-ext-install -j$(nproc) gd \
      && docker-php-source delete

COPY . /app

WORKDIR /app

RUN composer update

RUN php artisan key:generate
RUN npm --version
RUN npm install
RUN npm run production

EXPOSE 8000

CMD php artisan serve --host=0.0.0.0 --port=8000
