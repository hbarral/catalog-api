# Catalog API
![Catalog_API_Image](.docs/Catalog_API_with_Swagger.png "Catalog API with Swagger")

## Project Overview
This project aims to demonstrate the use of the Swagger tool to document the API of a product catalog, built with the support of the PHP Laravel framework.
You can see and try it live at [Catalog API](https://labs.catalog-api.hectorbarral.com/api/documentation)
## Configuration instructions
A computer with Linux system (It should work on Windows and Mac as well) capable of running Docker (as the first alternative).

## Technologies
* PHP 7.2
* Nginx 1.18.0
* MySQL 8
* npm 6.13.6

## Installation instructions
it can be done in two ways
* Docker instructions
* Normal instructions

### Docker instructions
* Export these variables as environment variables, ignoring ```bash: UID: readonly variable```
```bash
export UID=${UID}
export GID=${GID}
```

* Clone this repository
```bash
git clone git@bitbucket.org:hbarral/catalog-api.git
```

* cd into 'catalog-api'
```bash
cd catalog-api
```

* Run docker containers
```bash
docker-compose up -d --build
```

* Install composer packages
```bash
docker-compose run --rm composer install
```

* Copy .env file
```bash
cp .env.example .env
```

* Generate key
```bash
docker-compose exec fpm php /var/www/html/artisan key:generate
```

* Run migrations and seed
```bash
docker-compose exec fpm php /var/www/html/artisan migrate --seed
```

* Generate JWT secret key
```bash
docker-compose exec fpm php /var/www/html/artisan jwt:secret
```

* Install npm packages
```bash
docker-compose run --rm npm install
```

* Compile assets
```bash
docker-compose run --rm npm run dev
```

> Now you can see the project in the following url http://localhost:8000/api/documentation

### Normal instructions

* Clone project
```bash
git clone git@bitbucket.org:hbarral/catalog-api.git catalog-api
```

* cd into 'catalog-api'
```bash
cd catalog-api
```

* Install composer packages
```bash
composer install
```

* Copy .env file
```bash
cp .env.example .env
```

* Generate key
```bash
php artisan key:generate
```

* Run migrations and seed
```bash
php artisan migrate --seed
```

* Generate JWT secret key
```bash
php artisan jwt:secret
```

* Install npm packages
```bash
npm install
```

* Compile assets
```bash
npm run dev
```

* Run dev server
```bash
php artisan serve
```

> Now you can see the project in the following url http://localhost:8000/api/documentation

## Operating Instructions

In order to use the parts that require authentication, you first need to obtain the **Token**. As indicated in the following image.

![Login_1](.docs/login.png "Login")

You can use the proposed user.
![login2.png](.docs/login2.png "login 2")

Or previously register your own user (optional).
![register new user](.docs/register_new_user.png "register new user")
After login, you will get the **Token** in the response.
![response](.docs/response.png "response")

Copy the highlighted token as indicated in the image.
![token](.docs/token.png "token")

Go to the \"**Authorize**\" option as indicated in the image.
![authorize](.docs/authorize.png)

Open the dialog box and enter the **Token** as instructed. Then click \"**Authorize**\".
![input_token](.docs/input_token.png)

You are now an authenticated user.
![authenticated](.docs/authenticated.png)

You can try the API now !!!
![get_category](.docs/get_category.png)
