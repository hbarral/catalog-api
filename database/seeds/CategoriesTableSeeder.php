<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = ['TV', 'Laptops', 'Computers', 'Cellphones'];
        $faker = Faker::create();

        foreach ($categories as $category) {
            DB::table('categories')->insert([
          'name' => $category,
          'description' => $faker->paragraph,
          'user_id' => 1,
          'created_at' => $faker->dateTime('now'),
          'updated_at' => $faker->dateTime('now'),
        ]);
        }
    }
}
