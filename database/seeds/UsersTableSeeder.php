<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        'name' => 'John Doe',
        'email' => 'john.doe@mail.ru',
        'password' => bcrypt('1234567890')
      ]);
    }
}
