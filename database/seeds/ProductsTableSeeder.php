<?php

use App\Category;
use App\Product;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $allCategories = Category::where('id', '>', 0)->pluck('id')->toArray();

        foreach (range(1, 15) as $index) {
            $product = Product::create([
            'name'  =>  $faker->word,
            'price' =>  $faker->randomFloat(2, 10, 500),
            'description' => $faker->paragraph(),
            'user_id' => 1,
            'created_at' => $faker->dateTime('now'),
            'updated_at' => $faker->dateTime('now'),
            ]);
            $randomCategories = Arr::random($allCategories, 2);
            $categoriesSelected = Category::find($randomCategories);
            $product->categories()->attach($categoriesSelected);
        }
    }
}
